PKG_CONFIG = pkg-config
CFLAGS += -Wall -g $(shell $(PKG_CONFIG) --cflags libwbananui1)
LDFLAGS_UI += $(shell $(PKG_CONFIG) --libs libwbananui1)
OBJECTS = fmradio.o iris.o
ROUTINGOBJECTS = routing.o
OUTPUTS = bananui-fmradio hostless-routing-daemon

all: $(OUTPUTS)

bananui-fmradio: $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS_UI) $(CFLAGS)

hostless-routing-daemon: $(ROUTINGOBJECTS)
	$(CC) -o $@ $(ROUTINGOBJECTS) $(LDFLAGS) $(CFLAGS)

fmradio.o: radioUI.h iris.h
iris.o: iris.h

%.o: %.c
	$(CC) -o $@ -c $*.c $(CFLAGS)

clean:
	rm -f $(OBJECTS) $(ROUTINGOBJECTS) $(OUTPUTS)

install:
	mkdir -p $(DESTDIR)/usr/bin
	install $(OUTPUTS) $(DESTDIR)/usr/bin
	mkdir -p $(DESTDIR)/usr/share/applications
	install fmradio.desktop $(DESTDIR)/usr/share/applications/bananui-fmradio.desktop
