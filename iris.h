enum disp_ev {
	DISP_RADIO_ON,
	DISP_RADIO_OFF,
	DISP_RDS_ON,
	DISP_RDS_OFF,
	DISP_TEXT_PRI,
	DISP_TEXT_SEC,
	DISP_FREQ_UPDATE,
	DISP_SEEK_DONE,
};

typedef void (*display_callback_t)(void *, enum disp_ev, const char *);

int iris_seek(int fd, unsigned int low, unsigned int high, unsigned int up);
int iris_init(int fd, unsigned int *low, unsigned int *high);
int iris_freq(int fd, unsigned int freq);
unsigned int iris_get_freq(int fd);
int iris_evq(int fd, unsigned char *membuf, size_t *len);
int iris_evq_process(int fd, unsigned char *membuf, size_t len,
		display_callback_t cb, void *data);
int iris_disable(int fd);
