#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>
#include "iris.h"

enum v4l2_cid_private_iris_t {
	V4L2_CID_PRIVATE_IRIS_SRCHMODE = (0x08000000 + 1),
	V4L2_CID_PRIVATE_IRIS_SCANDWELL,
	V4L2_CID_PRIVATE_IRIS_SRCHON,
	V4L2_CID_PRIVATE_IRIS_STATE,
	V4L2_CID_PRIVATE_IRIS_TRANSMIT_MODE,
	V4L2_CID_PRIVATE_IRIS_RDSGROUP_MASK,
	V4L2_CID_PRIVATE_IRIS_REGION,
	V4L2_CID_PRIVATE_IRIS_SIGNAL_TH,
	V4L2_CID_PRIVATE_IRIS_SRCH_PTY,
	V4L2_CID_PRIVATE_IRIS_SRCH_PI,
	V4L2_CID_PRIVATE_IRIS_SRCH_CNT,
	V4L2_CID_PRIVATE_IRIS_EMPHASIS,
	V4L2_CID_PRIVATE_IRIS_RDS_STD,
	V4L2_CID_PRIVATE_IRIS_SPACING,
	V4L2_CID_PRIVATE_IRIS_RDSON,
	V4L2_CID_PRIVATE_IRIS_RDSGROUP_PROC,
	V4L2_CID_PRIVATE_IRIS_LP_MODE,
	V4L2_CID_PRIVATE_IRIS_ANTENNA,
	V4L2_CID_PRIVATE_IRIS_RDSD_BUF,
	V4L2_CID_PRIVATE_IRIS_PSALL,  /*0x8000014*/

	/*v4l2 Tx controls*/
	V4L2_CID_PRIVATE_IRIS_TX_SETPSREPEATCOUNT,
	V4L2_CID_PRIVATE_IRIS_STOP_RDS_TX_PS_NAME,
	V4L2_CID_PRIVATE_IRIS_STOP_RDS_TX_RT,
	V4L2_CID_PRIVATE_IRIS_IOVERC,
	V4L2_CID_PRIVATE_IRIS_INTDET,
	V4L2_CID_PRIVATE_IRIS_MPX_DCC,
	V4L2_CID_PRIVATE_IRIS_AF_JUMP,
	V4L2_CID_PRIVATE_IRIS_RSSI_DELTA,
	V4L2_CID_PRIVATE_IRIS_HLSI, /*0x800001d*/

	/*Diagnostic commands*/
	V4L2_CID_PRIVATE_IRIS_SOFT_MUTE,
	V4L2_CID_PRIVATE_IRIS_RIVA_ACCS_ADDR,
	V4L2_CID_PRIVATE_IRIS_RIVA_ACCS_LEN,
	V4L2_CID_PRIVATE_IRIS_RIVA_PEEK,
	V4L2_CID_PRIVATE_IRIS_RIVA_POKE,
	V4L2_CID_PRIVATE_IRIS_SSBI_ACCS_ADDR,
	V4L2_CID_PRIVATE_IRIS_SSBI_PEEK,
	V4L2_CID_PRIVATE_IRIS_SSBI_POKE,
	V4L2_CID_PRIVATE_IRIS_TX_TONE,
	V4L2_CID_PRIVATE_IRIS_RDS_GRP_COUNTERS,
	V4L2_CID_PRIVATE_IRIS_SET_NOTCH_FILTER, /* 0x8000028 */
	V4L2_CID_PRIVATE_IRIS_SET_AUDIO_PATH, /* TAVARUA specific command */
	V4L2_CID_PRIVATE_IRIS_DO_CALIBRATION,
	V4L2_CID_PRIVATE_IRIS_SRCH_ALGORITHM, /* TAVARUA specific command */
	V4L2_CID_PRIVATE_IRIS_GET_SINR,
	V4L2_CID_PRIVATE_INTF_LOW_THRESHOLD,
	V4L2_CID_PRIVATE_INTF_HIGH_THRESHOLD,
	V4L2_CID_PRIVATE_SINR_THRESHOLD,
	V4L2_CID_PRIVATE_SINR_SAMPLES,
	V4L2_CID_PRIVATE_SPUR_FREQ,
	V4L2_CID_PRIVATE_SPUR_FREQ_RMSSI,
	V4L2_CID_PRIVATE_SPUR_SELECTION,
	V4L2_CID_PRIVATE_UPDATE_SPUR_TABLE,
	V4L2_CID_PRIVATE_VALID_CHANNEL,
	V4L2_CID_PRIVATE_AF_RMSSI_TH,
	V4L2_CID_PRIVATE_AF_RMSSI_SAMPLES,
	V4L2_CID_PRIVATE_GOOD_CH_RMSSI_TH,
	V4L2_CID_PRIVATE_SRCHALGOTYPE,
	V4L2_CID_PRIVATE_CF0TH12,
	V4L2_CID_PRIVATE_SINRFIRSTSTAGE,
	V4L2_CID_PRIVATE_RMSSIFIRSTSTAGE,
	V4L2_CID_PRIVATE_RXREPEATCOUNT,
	V4L2_CID_PRIVATE_IRIS_RSSI_TH,
	V4L2_CID_PRIVATE_IRIS_AF_JUMP_RSSI_TH,
	V4L2_CID_PRIVATE_BLEND_SINRHI,
	V4L2_CID_PRIVATE_BLEND_RMSSIHI,

	/*using private CIDs under userclass*/
	V4L2_CID_PRIVATE_IRIS_READ_DEFAULT = 0x00980928,
	V4L2_CID_PRIVATE_IRIS_WRITE_DEFAULT,
	V4L2_CID_PRIVATE_IRIS_SET_CALIBRATION,
	V4L2_CID_PRIVATE_IRIS_SET_SPURTABLE = 0x0098092D,
	V4L2_CID_PRIVATE_IRIS_GET_SPUR_TBL  = 0x0098092E,
};
enum radio_state_t {
	FM_OFF,
	FM_RECV,
	FM_TRANS,
	FM_RESET,
	FM_CALIB,
	FM_TURNING_OFF,
	FM_RECV_TURNING_ON,
	FM_TRANS_TURNING_ON,
	FM_MAX_NO_STATES,
};

int iris_seek(int fd, unsigned int low, unsigned int high, unsigned int up)
{
	struct v4l2_hw_freq_seek seek = {
		.tuner = 0,
		.type = V4L2_TUNER_RADIO,
		.seek_upward = up,
		.wrap_around = 0,
		.spacing = 100,
		.rangelow = low,
		.rangehigh = high
	};
	if(ioctl(fd, VIDIOC_S_HW_FREQ_SEEK, &seek)){
		perror("hw seek failed");
		return 1;
	}
	return 0;
}

static int iris_stereo(int fd)
{
	struct v4l2_tuner tuner = {
		.index = 0,
		.audmode = V4L2_TUNER_MODE_STEREO,
		.rangelow = 1400000,
		.rangehigh = 1728000,
	};
	if(ioctl(fd, VIDIOC_S_TUNER, &tuner)){
		perror("VIDIOC_S_TUNER");
		return 1;
	}
	return 0;
}

static int iris_volume(int fd)
{
	struct v4l2_control control = {
		.id = V4L2_CID_AUDIO_VOLUME
	};
	if(ioctl(fd, VIDIOC_G_CTRL, &control)){
		perror("VIDIOC_G_CTRL (AUDIO_VOLUME)");
		return 1;
	}
	printf("Audio volume: %u -> 50\n", control.value);
	control.value = 50;
	if(ioctl(fd, VIDIOC_S_CTRL, &control)){
		perror("VIDIOC_S_CTRL (AUDIO_VOLUME)");
		return 1;
	}
	return 0;
}

int iris_freq(int fd, unsigned int freq)
{
	struct v4l2_frequency frequency = {
		.tuner = 0,
		.type = V4L2_TUNER_RADIO,
		.frequency = freq
	};
	printf("Setting frequency: %f MHz\n", freq / 16000.0);
	if(ioctl(fd, VIDIOC_S_FREQUENCY, &frequency)){
		perror("VIDIOC_S_FREQUENCY");
		return 1;
	}
	return 0;
}

unsigned int iris_get_freq(int fd)
{
	struct v4l2_frequency frequency = {
		.tuner = 0,
		.type = V4L2_TUNER_RADIO
	};
	if(ioctl(fd, VIDIOC_G_FREQUENCY, &frequency)){
		perror("VIDIOC_S_FREQUENCY");
		return 0;
	}
	printf("Got frequency: %f MHz\n", frequency.frequency / 16000.0);
	return frequency.frequency;
}

enum iris_buf_t {
	IRIS_BUF_SRCH_LIST,
	IRIS_BUF_EVENTS,
	IRIS_BUF_RT_RDS,
	IRIS_BUF_PS_RDS,
	IRIS_BUF_RAW_RDS,
	IRIS_BUF_AF_LIST,
	IRIS_BUF_PEEK,
	IRIS_BUF_SSBI_PEEK,
	IRIS_BUF_RDS_CNTRS,
	IRIS_BUF_RD_DEFAULT,
	IRIS_BUF_CAL_DATA,
	IRIS_BUF_RT_PLUS,
	IRIS_BUF_ERT,
	IRIS_BUF_SPUR,
	IRIS_BUF_MAX,
};

enum iris_evt_t {
	IRIS_EVT_RADIO_READY,
	IRIS_EVT_TUNE_SUCC,
	IRIS_EVT_SEEK_COMPLETE,
	IRIS_EVT_SCAN_NEXT,
	IRIS_EVT_NEW_RAW_RDS,
	IRIS_EVT_NEW_RT_RDS,
	IRIS_EVT_NEW_PS_RDS,
	IRIS_EVT_ERROR,
	IRIS_EVT_BELOW_TH,
	IRIS_EVT_ABOVE_TH,
	IRIS_EVT_STEREO,
	IRIS_EVT_MONO,
	IRIS_EVT_RDS_AVAIL,
	IRIS_EVT_RDS_NOT_AVAIL,
	IRIS_EVT_NEW_SRCH_LIST,
	IRIS_EVT_NEW_AF_LIST,
	IRIS_EVT_TXRDSDAT,
	IRIS_EVT_TXRDSDONE,
	IRIS_EVT_RADIO_DISABLED,
	IRIS_EVT_NEW_ODA,
	IRIS_EVT_NEW_RT_PLUS,
	IRIS_EVT_NEW_ERT,
	IRIS_EVT_SPUR_TBL,
};

static int iris_rds(int fd, unsigned int bufidx,
	display_callback_t cb, void *data)
{
	unsigned char membuf[256];
	int i;
	struct v4l2_buffer buf = {
		.type = V4L2_BUF_TYPE_PRIVATE,
		.index = bufidx,
		.memory = V4L2_MEMORY_USERPTR,
		.m = {
			.userptr = (unsigned long int)membuf
		},
		.length = 255
	};
	memset(&membuf, 0, 256);
	if(ioctl(fd, VIDIOC_DQBUF, &buf)){
		perror("VIDIOC_DQBUF (RDS)");
		return 1;
	}
	printf("%s", bufidx == IRIS_BUF_RT_RDS
			? "RT" : bufidx == IRIS_BUF_PS_RDS
			? "PS" : bufidx == IRIS_BUF_RT_PLUS
			? "RT+" : bufidx == IRIS_BUF_RAW_RDS
			? "RAW" : "UNK");
	if(bufidx == IRIS_BUF_RT_RDS)
		cb(data, DISP_TEXT_SEC, (char*)membuf+5);
	else if(bufidx == IRIS_BUF_PS_RDS)
		cb(data, DISP_TEXT_PRI, (char*)membuf+5);
	else for(i = 0; i < buf.bytesused; i++){
		printf(" %02x", membuf[i]);
	}
	printf("\n");
	return 0;
}

int iris_evq(int fd, unsigned char *membuf, size_t *len)
{
	struct v4l2_buffer buf = {
		.type = V4L2_BUF_TYPE_PRIVATE,
		.index = IRIS_BUF_EVENTS,
		.memory = V4L2_MEMORY_USERPTR,
		.m = {
			.userptr = (unsigned long int)membuf
		},
		.length = 256
	};
	if(ioctl(fd, VIDIOC_DQBUF, &buf)){
		perror("VIDIOC_DQBUF");
		return 1;
	}
	*len = buf.bytesused;
	return 0;
}

int iris_evq_process(int fd, unsigned char *membuf, size_t len,
		display_callback_t cb, void *data)
{
	int i;
	for(i = 0; i < len; i++){
		switch(membuf[i]){
			case IRIS_EVT_SEEK_COMPLETE:
				cb(data, DISP_SEEK_DONE, NULL); break;
			case IRIS_EVT_NEW_RAW_RDS:
				if(iris_rds(fd, IRIS_BUF_RAW_RDS, cb, data))
					return 1;
				break;
			case IRIS_EVT_NEW_RT_RDS:
				if(iris_rds(fd, IRIS_BUF_RT_RDS, cb, data))
					return 1;
				break;
			case IRIS_EVT_NEW_RT_PLUS:
				if(iris_rds(fd, IRIS_BUF_RT_PLUS, cb, data))
					return 1;
				break;
			case IRIS_EVT_NEW_PS_RDS:
				if(iris_rds(fd, IRIS_BUF_PS_RDS, cb, data))
					return 1;
				break;
			case IRIS_EVT_RDS_AVAIL:
				cb(data, DISP_RDS_ON, NULL); break;
			case IRIS_EVT_RDS_NOT_AVAIL:
				cb(data, DISP_RDS_OFF, NULL); break;
			case IRIS_EVT_RADIO_READY:
				cb(data, DISP_RADIO_ON, NULL); break;
			case IRIS_EVT_TUNE_SUCC:
				cb(data, DISP_FREQ_UPDATE, NULL); break;
			case IRIS_EVT_SCAN_NEXT:
				printf("IRIS_EVT_SCAN_NEXT\n"); break;
			case IRIS_EVT_ERROR:
				printf("IRIS_EVT_ERROR\n"); break;
			case IRIS_EVT_BELOW_TH:
				printf("IRIS_EVT_BELOW_TH\n"); break;
			case IRIS_EVT_ABOVE_TH:
				printf("IRIS_EVT_ABOVE_TH\n"); break;
			case IRIS_EVT_STEREO:
				printf("IRIS_EVT_STEREO\n"); break;
			case IRIS_EVT_MONO:
				printf("IRIS_EVT_MONO\n"); break;
			case IRIS_EVT_NEW_SRCH_LIST:
				printf("IRIS_EVT_NEW_SRCH_LIST\n"); break;
			case IRIS_EVT_NEW_AF_LIST:
				printf("IRIS_EVT_NEW_AF_LIST\n"); break;
			case IRIS_EVT_TXRDSDAT:
				printf("IRIS_EVT_TXRDSDAT\n"); break;
			case IRIS_EVT_TXRDSDONE:
				printf("IRIS_EVT_TXRDSDONE\n"); break;
			case IRIS_EVT_RADIO_DISABLED:
				cb(data, DISP_RADIO_OFF, NULL); break;
			case IRIS_EVT_NEW_ODA:
				printf("IRIS_EVT_NEW_ODA\n"); break;
			case IRIS_EVT_NEW_ERT:
				printf("IRIS_EVT_NEW_ERT\n"); break;
			case IRIS_EVT_SPUR_TBL:
				printf("IRIS_EVT_SPUR_TBL\n"); break;
		}
	}
	return 0;
}

static int iris_rdson(int fd)
{
	struct v4l2_control control = {
		.id = V4L2_CID_PRIVATE_IRIS_RDS_STD,
		.value = 2
	};
	if(ioctl(fd, VIDIOC_S_CTRL, &control)){
		perror("VIDIOC_S_CTRL (IRIS_RDS_STD)");
		return 1;
	}
	control.id = V4L2_CID_PRIVATE_IRIS_PSALL;
	control.value = 1;
	if(ioctl(fd, VIDIOC_S_CTRL, &control)){
		perror("VIDIOC_S_CTRL (IRIS_PSALL)");
		return 1;
	}
	control.id = V4L2_CID_PRIVATE_IRIS_RDSGROUP_PROC;
	control.value = 0xff;
	if(ioctl(fd, VIDIOC_S_CTRL, &control)){
		perror("VIDIOC_S_CTRL (IRIS_RDSGROUP_PROC)");
		return 1;
	}
	control.id = V4L2_CID_PRIVATE_IRIS_RDSGROUP_MASK;
	control.value = 0xff;
	if(ioctl(fd, VIDIOC_S_CTRL, &control)){
		perror("VIDIOC_S_CTRL (IRIS_RDSGROUP_MASK)");
		return 1;
	}
	control.id = V4L2_CID_PRIVATE_IRIS_RDSON;
	control.value = 1;
	if(ioctl(fd, VIDIOC_S_CTRL, &control)){
		perror("VIDIOC_S_CTRL (IRIS_RDSON)");
		return 1;
	}
	return 0;
}

static int iris_state(int fd, int trans)
{
	struct v4l2_control control = {
		.id = V4L2_CID_PRIVATE_IRIS_STATE
	};
	const char *str;
	if(ioctl(fd, VIDIOC_G_CTRL, &control)){
		perror("VIDIOC_G_CTRL (IRIS_STATE)");
		return 1;
	}
	switch(control.value){
		case FM_OFF:		str = "FM_OFF"; break;
		case FM_RECV:		str = "FM_RECV"; break;
		case FM_TRANS:		str = "FM_TRANS"; break;
		case FM_RESET:		str = "FM_RESET"; break;
		case FM_CALIB:		str = "FM_CALIB"; break;
		case FM_TURNING_OFF:	str = "FM_TURNING_OFF"; break;
		case FM_RECV_TURNING_ON:str = "FM_RECV_TURNING_ON"; break;
		case FM_TRANS_TURNING_ON:str= "FM_TRANS_TURNING_ON"; break;
		default:		str = "INVALID"; break;
	}
	printf("Radio state was %d (%s), enabling %s\n", control.value, str,
			trans ? "FM_TRANS" : "FM_RECV");
	control.value = trans ? FM_TRANS : FM_RECV;
	if(ioctl(fd, VIDIOC_S_CTRL, &control)){
		perror("VIDIOC_S_CTRL (IRIS_STATE)");
		return 1;
	}
	return 0;
}

static int iris_attach_transport(void)
{
	int fd = open("/sys/module/radio_iris_transport/parameters/fmsmd_set",
			O_WRONLY);
	if(fd < 0){
		perror("open fmsmd_set");
		return 1;
	}
	if(write(fd, "1", 1) < 0){
		perror("write fmsmd_set");
		return 1;
	}
	close(fd);
	sleep(1); /* Wait for driver */
	return 0;
}

int iris_init(int fd, unsigned int *low, unsigned int *high)
{
	struct v4l2_tuner tuner = {
		.index = 0
	};
	if(iris_attach_transport()) return 1;
	if(iris_state(fd, 0)) return 1;
	if(iris_rdson(fd)) return 1;
	if(iris_stereo(fd)) return 1;
	if(iris_volume(fd)) return 1;
	if(ioctl(fd, VIDIOC_G_TUNER, &tuner)){
		perror("VIDIOC_G_TUNER");
		return 2;
	}
	*high = tuner.rangehigh;
	*low = tuner.rangelow;
	return 0;
}

int iris_disable(int fd)
{
	struct v4l2_control control = {
		.id = V4L2_CID_PRIVATE_IRIS_STATE,
		.value = FM_OFF
	};
	if(ioctl(fd, VIDIOC_S_CTRL, &control)){
		perror("VIDIOC_S_CTRL (IRIS_STATE = FM_OFF)");
		return 1;
	}
	return 0;
}
