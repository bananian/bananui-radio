#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sound/asound.h>

#define FIRST_MASK SNDRV_PCM_HW_PARAM_FIRST_MASK
#define FIRST_INTERVAL SNDRV_PCM_HW_PARAM_FIRST_INTERVAL

static void set_hwparams(int fd)
{
	struct snd_pcm_hw_params hwparams = {
		.flags = 0,
		.masks = {
			[SNDRV_PCM_HW_PARAM_ACCESS - FIRST_MASK] = {
				.bits = {
					1U << SNDRV_PCM_ACCESS_RW_INTERLEAVED
				}
			},
			[SNDRV_PCM_HW_PARAM_FORMAT - FIRST_MASK] = {
				.bits = {
					1U << SNDRV_PCM_FORMAT_S16_LE
				}
			},
			[SNDRV_PCM_HW_PARAM_SUBFORMAT - FIRST_MASK] = {
				.bits = {
					1U << SNDRV_PCM_SUBFORMAT_STD
				}
			}
		},
		.intervals = {
			[SNDRV_PCM_HW_PARAM_SAMPLE_BITS - FIRST_INTERVAL] = {
				.min = 16, .max = 16,
				.openmin = 0, .openmax = 0,
				.integer = 1, .empty = 0
			},
			[SNDRV_PCM_HW_PARAM_FRAME_BITS - FIRST_INTERVAL] = {
				.min = 16, .max = 16,
				.openmin = 0, .openmax = 0,
				.integer = 1, .empty = 0
			},
			[SNDRV_PCM_HW_PARAM_CHANNELS - FIRST_INTERVAL] = {
				.min = 1, .max = 1,
				.openmin = 0, .openmax = 0,
				.integer = 1, .empty = 0
			},
			[SNDRV_PCM_HW_PARAM_RATE - FIRST_INTERVAL] = {
				.min = 8000, .max = 8000,
				.openmin = 0, .openmax = 0,
				.integer = 1, .empty = 0
			},
			[SNDRV_PCM_HW_PARAM_PERIOD_TIME - FIRST_INTERVAL] = {
				.min = 125000, .max = 125000,
				.openmin = 0, .openmax = 0,
				.integer = 1, .empty = 0
			},
			[SNDRV_PCM_HW_PARAM_PERIOD_SIZE - FIRST_INTERVAL] = {
				.min = 1000, .max = 1000,
				.openmin = 0, .openmax = 0,
				.integer = 1, .empty = 0
			},
			[SNDRV_PCM_HW_PARAM_PERIOD_BYTES - FIRST_INTERVAL] = {
				.min = 2000, .max = 2000,
				.openmin = 0, .openmax = 0,
				.integer = 1, .empty = 0
			},
			[SNDRV_PCM_HW_PARAM_PERIODS - FIRST_INTERVAL] = {
				.min = 4, .max = 4,
				.openmin = 0, .openmax = 0,
				.integer = 1, .empty = 0
			},
			[SNDRV_PCM_HW_PARAM_BUFFER_TIME - FIRST_INTERVAL] = {
				.min = 500000, .max = 500000,
				.openmin = 0, .openmax = 0,
				.integer = 1, .empty = 0
			},
			[SNDRV_PCM_HW_PARAM_BUFFER_SIZE - FIRST_INTERVAL] = {
				.min = 4000, .max = 4000,
				.openmin = 0, .openmax = 0,
				.integer = 1, .empty = 0
			},
			[SNDRV_PCM_HW_PARAM_BUFFER_BYTES - FIRST_INTERVAL] = {
				.min = 8000, .max = 8000,
				.openmin = 0, .openmax = 0,
				.integer = 1, .empty = 0
			},
			[SNDRV_PCM_HW_PARAM_TICK_TIME - FIRST_INTERVAL] = {
				.min = 0, .max = 0,
				.openmin = 0, .openmax = 0,
				.integer = 1, .empty = 0
			},
		},
		.rmask = ~0U
	};
	if(ioctl(fd, SNDRV_PCM_IOCTL_HW_PARAMS, &hwparams)){
		perror("set hw params failed");
		return;
	}
	if(ioctl(fd, SNDRV_PCM_IOCTL_PREPARE)){
		perror("prepare pcm failed");
		return;
	}
}

int main(int argc, char * const *argv)
{
	int cfd, pfd;
	if(argc < 3){
		fprintf(stderr, "Usage: hostless-routing-daemon <in> <out>\n");
		return 1;
	}
	cfd = open(argv[1], O_RDONLY);
	if(cfd < 0){
		perror(argv[1]);
		return 1;
	}
	pfd = open(argv[2], O_RDONLY);
	if(pfd < 0){
		perror(argv[2]);
		return 1;
	}
	fprintf(stderr, "Setting hw params for %s...\n", argv[1]);
	set_hwparams(cfd);
	fprintf(stderr, "Setting hw params for %s...\n", argv[2]);
	set_hwparams(pfd);
	while(1) pause();
	return 255;
}
